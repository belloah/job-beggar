import React, { useState } from 'react';
// import { useEffect } from 'react';
import ReactDOM from 'react-dom';
import Footer from './Footer';
import Main from './Main';
import Release from './Release';
import Title from './Title';
import Collapse from 'react-bootstrap/Collapse';
import 'bootswatch/dist/journal/bootstrap.min.css'
import './index.css';
// import reportWebVitals from './reportWebVitals';

const name = "Job Beggar"

const Jobbeggar = () => {
  const [display, setDisplay] = useState(false)
  const [lang, setLang] = useState(false)
  const [subject, setSubject] = useState("")
  const [content, setContent] = useState("")
  const [send, setSend] = useState("")
  const bananas = [
    { id: "name", label: "Name", help: "HOM Ka Ling", state: useState(""), clear: useState(false) },
    { id: "position", label: "Position", help: "量地官", state: useState(""), clear: useState(false) },
    { id: "source", label: "Source name", help: "Parttime.hk, JobsDB, 兼職網", state: useState(""), clear: useState(false) },
    { id: "link", label: "Source link", help: "http://www.parttime.hk/job/123456", state: useState(""), clear: useState(false) },
    { id: "recipent", label: "Recipient", help: "Miss HOM, Mr HOM, default Sir/Madam in English", state: useState(""), clear: useState(false) },
    { id: "receiver", label: "To", help: "csbcomp@csb.gov.hk", state: useState(""), clear: useState(false) }
  ];
  // eslint-disable-next-line
  const eat = () => bananas.map(item => {
    if (item.clear[0] === false) item.state[1]("")
  })
  const go = () => {
    if (bananas.slice(0, 4).some(item => item.state[0] === "")) { }
    else {
      const mailsubject = lang ? "申請 " + bananas[1].state[0] + " - " + bananas[0].state[0] : "Application for " + bananas[1].state[0] + " - " + bananas[0].state[0]
      const mailcontent = lang
        ? `${bananas[4].state[0] === "" ? "" : `${bananas[4].state[0]}：

`}你好，在${bananas[2].state[0]} (${bananas[3].state[0]}) 看到了招聘訊息，隨函附上履歷表，謝謝。`
        : `${bananas[4].state[0] === "" ? "Dear Sir/Madam" : "Dear " + bananas[4].state[0]},

I am writing to apply for the position of ${bananas[1].state[0]} advertised on ${bananas[2].state[0]} (${bananas[3].state[0]}).

Enclosed is a copy of my resume which provides details of my qualifications for the position. I would appreciate the opportunity to meet with you to discuss my interest and qualifications. I look forward to hearing from you soon.

Yours ${bananas[4].state[0] === "" ? "faithfully" : "sincerely"},
${bananas[0].state[0]}`

      setSubject(mailsubject)
      setContent(mailcontent)
      setSend(bananas[5].state[0] === "" ? "" : <a href={"mailto:" + bananas[5].state[0] + "?subject=" + encodeURIComponent(mailsubject) + "&body=" + encodeURIComponent(mailcontent)} className="btn btn-success btn-block">Para tu papoy!</a>)
      setDisplay(true)
    }
  }
  const copy = (e) => {
    e.target.select()
    document.execCommand("copy")
  }
  // testing values
  // const dummy = ""
  // useEffect(() => {
  //   const test = ["HOM Ka Ling", "hehe", "Parttime.hk", "http://www.parttime.hk/job/123456", "Miss HOM", "homkaling@belloah.gitlab.io"]
  //   bananas.map((item, index) => item.state[1](test[index]))
  //   bananas.slice(0, 4).map(item => item.clear[1](true))
  //   // eslint-disable-next-line
  // }, dummy)

  return (<>
    <div className="card card-body">
      <div className="card-title">Put bananas here:</div>
      <div className="card-text">
        {bananas.map((item, key) => <div className="form-group row" key={key}>
          <label htmlFor={item.id} className="col-lg-2 col-form-label">{item.label}</label>
          <div className="col-lg-10">
            <input type="text" name={item.id} className="form-control" value={item.state[0]} onInput={e => item.state[1](e.target.value)} />
            {item.help ? <small className="help-text">e.g. {item.help}</small> : ""}
          </div>
        </div>)}

        <div className="custom-control custom-checkbox mb-3">
          <input type="checkbox" className="custom-control-input" id="lang" name="lang" checked={lang} onChange={e => setLang(e.target.checked)} />
          <label className="custom-control-label" htmlFor="lang">Chinese</label>
        </div>

        <div className="form-group row">
          <div className="col">
            <button className="btn btn-success form-control btn-lg" onClick={go}>Go bananas!</button>
          </div>
          <div className="col">
            <button className="btn btn-secondary form-control btn-lg" onClick={eat}>Eat bananas!</button>
          </div>
        </div>
        <div className="form-group d-flex flex-column flex-lg-row alert alert-warning">
          <div className="mr-3">Stop minion from eating them:</div>
          {bananas.map((item, key) => (<div className="custom-control custom-checkbox custom-control-inline" key={key}>
            <input type="checkbox" className="custom-control-input" id={"c" + item.id} name={"c" + item.id} value={item.id} checked={item.clear[0]} onChange={e => item.clear[1](e.target.checked)} />
            <label className="custom-control-label" htmlFor={"c" + item.id}>{item.label}</label>
          </div>))}
        </div>

      </div>
    </div>
    <Collapse className="card my-3 collapse" in={display}>
      <div className="card-body">
        <div className="card-title">Here is the response from minion:</div>
        <div className="card-text">
          <div className="form-group row">
            <label htmlFor="subject" className="col-lg-2 col-form-label">Subject</label>
            <div className="col-lg-10"><input type="text" className="form-control" id="subject" readOnly value={subject} onClick={copy} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="content" className="col-lg-2 col-form-label">Content</label>
            <div className="col-lg-10">
              <textarea className="form-control" id="content" style={{ height: "25vh" }} value={content} readOnly onClick={copy} />
            </div>
          </div>
          {send}
        </div>
      </div>
    </Collapse>
  </>)
}

ReactDOM.render(
  <>
    <Main>
      <Title>{name}</Title>
      <Jobbeggar />
    </Main>
    <Footer>
      <Release name={name} />
    </Footer>
  </>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
